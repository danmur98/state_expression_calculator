package com.epam.rd.autotasks.springstatefulcalc;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

@RestController
public class CalculatorController {

    private final Map<String, Object> variables = new HashMap<>();
    private String expression;

    @PutMapping("/calc/expression")
    public ResponseEntity<String> setExpression(@RequestBody String expression) {
        if (isValidExpression(expression)) {
            this.expression = expression;
            return ResponseEntity.status(HttpStatus.CREATED).header("Location", "/calc/expression").build();
        } else {
            return ResponseEntity.badRequest().body("Invalid expression");
        }
    }

    @PutMapping("/calc/{variableName}")
    public ResponseEntity<String> setVariable(@PathVariable String variableName, @RequestBody String variableValue) {
        if (!isValidVariableName(variableName)) {
            return ResponseEntity.badRequest().body("Invalid variable name");
        }
        if (!isValidVariableValue(variableValue)) {
            return ResponseEntity.badRequest().body("Invalid variable value");
        }
        variables.put(variableName, variableValue);
        return ResponseEntity.status(HttpStatus.CREATED).header("Location", "/calc/" + variableName).build();
    }

    @GetMapping("/calc/result")
    public ResponseEntity<?> evaluateExpression(HttpSession session) {
        if (expression == null || !canEvaluateExpression()) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body("Cannot evaluate expression");
        }
        int result = evaluate(expression);
        return ResponseEntity.ok(result);
    }

    @DeleteMapping("/calc/expression")
    public ResponseEntity<Void> unsetExpression() {
        expression = null;
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("/calc/{variableName}")
    public ResponseEntity<Void> unsetVariable(@PathVariable String variableName) {
        variables.remove(variableName);
        return ResponseEntity.noContent().build();
    }

    private boolean isValidExpression(String expression) {
        // Add your validation logic here
        return true;
    }

    private boolean isValidVariableName(String variableName) {
        // Add your validation logic here
        return true;
    }

    private boolean isValidVariableValue(String variableValue) {
        // Add your validation logic here
        return true;
    }

    private boolean canEvaluateExpression() {
        // Add your logic to determine if the expression can be evaluated
        return true;
    }

    private int evaluate(String expression) {
        // Add your expression evaluation logic here
        return 0;
    }
}
 /*private final ExpressionEvaluator expressionEvaluator;
    private final Map<String, Integer> variables;

    public CalculatorController() {
        expressionEvaluator = new ExpressionEvaluator();
        variables = new HashMap<>();
    }

    @PostConstruct
    public void init() {
        expressionEvaluator.setVariables(variables);
    }

    @PutMapping("/expression")
    public ResponseEntity<?> setExpression(@RequestBody String expression) {
        if (!expressionEvaluator.isValidExpression(expression)) {
            return ResponseEntity.badRequest().body("Invalid expression");
        }

        expressionEvaluator.setExpression(expression);
        return ResponseEntity.status(HttpStatus.CREATED).header("Location", "/calc/expression").build();
    }

    @PutMapping("/{variableName}")
    public ResponseEntity<?> setVariable(@PathVariable String variableName, @RequestBody String variableValue) {
        if (!expressionEvaluator.isValidVariableName(variableName)) {
            return ResponseEntity.badRequest().body("Invalid variable name");
        }

        if (!expressionEvaluator.isValidVariableValue(variableValue)) {
            return ResponseEntity.badRequest().body("Invalid variable value");
        }

        int value;
        try {
            value = Integer.parseInt(variableValue);
        } catch (NumberFormatException e) {
            value = variables.get(variableValue);
        }

        variables.put(variableName, value);
        expressionEvaluator.setVariable(variableName, value);

        return ResponseEntity.status(HttpStatus.CREATED).header("Location", "/calc/" + variableName).build();
    }

    @GetMapping("/result")
    public ResponseEntity<?> getResult() {
        if (!expressionEvaluator.canEvaluateExpression()) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body("Cannot evaluate expression");
        }

        int result = expressionEvaluator.evaluate(expressionEvaluator.getExpression());
        return ResponseEntity.ok().body(result);
    }

    @DeleteMapping("/expression")
    public ResponseEntity<?> unsetExpression() {
        expressionEvaluator.unsetExpression();
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("/{variableName}")
    public ResponseEntity<?> unsetVariable(@PathVariable String variableName) {
        if (!expressionEvaluator.isValidVariableName(variableName)) {
            return ResponseEntity.badRequest().body("Invalid variable name");
        }

        variables.remove(variableName);
        expressionEvaluator.unsetVariable(variableName);

        return ResponseEntity.noContent().build();
    }

}*/
